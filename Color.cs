﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace TXCobalt.Core
{
    [Serializable]
    public struct Color
    {
        // Constructors
        public Color(byte R, byte G, byte B)
        {
            Red = R;
            Green = G;
            Blue = B;
            Alpha = 255;
        }
        public Color(byte R, byte G, byte B, byte A)
        {
            Red = R;
            Green = G;
            Blue = B;
            Alpha = A;
        }

        // Properties
        /// <summary>   
        /// Récupère ou défini une couleur sachant que:
        /// 0 est le Rouge, 1 est le vert
        /// 2 est le Bleu et 3 est l'opacité.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public byte this[int i]
        {
            get
            {
                return new [] { Red, Green, Blue, Alpha }[i];
            }
            set
            {
                if (i == 0)
                    Red = value;
                else if (i == 1)
                    Green = value;
                else if (i == 2)
                    Blue = value;
                else if (i == 3)
                    Alpha = value;
                else
                    throw new ArgumentOutOfRangeException("value");
            }
        }

        // Primitives
        public static Color red { get { return new Color(255, 0, 0); } }
        public static Color green { get { return new Color(0, 255, 0); } }
        public static Color blue { get { return new Color(0, 0, 255); } }

        // Monochromes
        public static Color White { get { return new Color(255,255,255); } }
        public static Color Black { get { return new Color(0, 0, 0); } }
        public static Color Gray { get { return new Color(127, 127, 127); } }

        // Secondaries
        public static Color Yellow { get { return new Color(255, 255, 0); } }
        public static Color Cyan { get { return new Color(0, 255, 255); } }
        public static Color Magenta { get { return new Color(255, 0, 255); } }
        
        // Variables
        public byte Red, Green, Blue, Alpha;

        // Fonctions
        /// <summary>
        /// Convertit une couleur au format ARGB vers une couleur.
        /// </summary>
        public static Color FromArgb(int argb)
        {
            byte[] bytes = BitConverter.GetBytes(argb);
            byte Alpha = bytes[0];
            byte Red = bytes[1];
            byte Green = bytes[2];
            byte Blue = bytes[3];

            return new Color(Red, Green, Blue, Alpha);
        }

        /// <summary>
        /// Convertit une couleur au format RGB vers une couleur.
        /// </summary>
        public static Color FromRgb(int rgb)
        {
            byte[] bytes = BitConverter.GetBytes(rgb);
            byte Red = bytes[0];
            byte Green = bytes[1];
            byte Blue = bytes[2];

            return new Color(Red, Green, Blue);
        }

        /// <summary>
        /// Convertit une couleur en format RGB.
        /// </summary>
        public static int ToRgb(Color color)
        {
            byte[] bytes = { color.Red, color.Green, color.Blue };
            return BitConverter.ToInt32(bytes, 0);
        }

        /// <summary>
        /// Convertit une couleur en format ARGB.
        /// </summary>
        public static int ToArgb(Color color)
        {
            byte[] bytes = { color.Alpha, color.Red, color.Green, color.Blue };
            return BitConverter.ToInt32(bytes, 0);
        }

        /// <summary>
        /// Convertit la couleur actuelle en format RGB.
        /// </summary>
        public int ToRgb()
        {
            return ToRgb(this);
        }

        /// <summary>
        /// Convertit la couleur actuelle en format ARGB.
        /// </summary>
        public int ToArgb()
        {
            return ToArgb(this);
        }

        public static string ToHex(Color color)
        {
            return Convert.ToString(ToRgb(color), 16);
        }

        public string ToHex()
        {
            return ToHex(this);
        }

        public override string ToString()
        {
            return "#" + ToHex(this);
        }

    }
}
