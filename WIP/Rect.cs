﻿using System;

namespace TXCobalt.Core.WIP
{
    struct Rect
    {
        // Contructor
        public Rect(Vector2D A, Vector2D B)
        {
            PointA = A;
            PointB = B;
        }
        // Fields
        public Vector2D PointA, PointB;
    }
}
