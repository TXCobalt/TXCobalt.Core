﻿

﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace TXCobalt.Core
{
    public static class ReflectionUtilities
    {
        public static Dictionary<string, object> ToDictionnary(object obj)
        {
            var fields_dictionnary = new Dictionary<string, object>();

            Type ObjType = obj.GetType();
            FieldInfo[] fields = ObjType.GetFields();

            foreach (FieldInfo field in fields)
                // Add to dictionnary only serializables and non-static types.
                if (!field.IsStatic && field.GetCustomAttributes(typeof(NonSerializedAttribute), false).Length == 0)
                    // Add directly to dictionnary Primitives (include string).
                    if (field.FieldType.IsPrimitive || field.FieldType == typeof(string))
                        fields_dictionnary.Add(field.Name, field.GetValue(obj));
            // If it is an enum, use underline value.
                    else if (field.FieldType.IsEnum)
                        fields_dictionnary.Add(field.Name, Convert.ChangeType(field.GetValue(obj), field.FieldType.GetEnumUnderlyingType()));
            // If it is an array, every their objects must be converted to a dictionnary or something else.
                    else if (field.FieldType.IsArray)
                        fields_dictionnary.Add(field.Name, ToRawData(((IEnumerable)field.GetValue(obj)).Cast<object>().ToArray(), field.FieldType.GetElementType()));
            // This is not a primitive or an array so, convert it to Dictionnary and add it.
                    else
                        fields_dictionnary.Add(field.Name, ToDictionnary(field.GetValue(obj)));

            return fields_dictionnary;
        }

        public static object ToObject(Dictionary<string,object> dictionnary, Type type)
        {
            object Obj = Activator.CreateInstance(type);

            foreach (var keyvaluepair in dictionnary)
                // Convert to object if this is a sub-dictionnary
                if (keyvaluepair.Value is Dictionary<string,object> && type.GetField(keyvaluepair.Key)?.FieldType != typeof(object))
                    type.GetField(keyvaluepair.Key).SetValue(Obj, ToObject((Dictionary<string,object>)keyvaluepair.Value, type.GetField(keyvaluepair.Key).FieldType));
                else if (keyvaluepair.Value is object[])
                    type.GetField(keyvaluepair.Key).SetValue(Obj, ToArray((object[])keyvaluepair.Value, type.GetField(keyvaluepair.Key).FieldType.GetElementType()));
                else
                    type.GetField(keyvaluepair.Key).SetValue(Obj, keyvaluepair.Value);

            return Obj;
        }

        /// <summary>
        /// Convert any array to object array.
        /// </summary>
        public static object[] ToRawData(object[] data, Type arraytype)
        {
            var objs = new List<object>();

            if (arraytype.IsPrimitive || arraytype == typeof(string))
                foreach (var obj in data)
                    objs.Add(obj);
            else if (arraytype.IsArray)
                foreach (var obj in data)
                    objs.Add(ToRawData(((IEnumerable)obj).Cast<object>().ToArray(), arraytype));
            else
                foreach (var obj in data)
                    objs.Add(ToDictionnary(obj));

            return objs.ToArray();
        }

        /// <summary>
        /// Convert an object array to an array with specified type.
        /// </summary>
        public static object ToArray(object[] rawdata, Type arraytype)
        {
            Array array = Array.CreateInstance(arraytype, rawdata.Length);

            for (int i = 0; i < rawdata.Length; i++)
                if (arraytype.IsPrimitive || arraytype == typeof(string))
                    array.SetValue(rawdata[i], i);
                else if (arraytype.IsArray)
                    array.SetValue(ToArray((object[])rawdata[i], arraytype), i);
                else if (arraytype.IsEnum)
                    array.SetValue(Enum.ToObject(arraytype, rawdata[i]), i);
                else
                    array.SetValue(ToObject((Dictionary<string,object>)rawdata[i], arraytype), i);

            return array;
        }
    }
}

