﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.IO;

namespace TXCobalt.Core
{
    /// <summary>
    /// Fourni un systeme de logging pour le deboggage.
    /// </summary>
    public static class Log
    {
        public static bool UseLogFile = true;
        public static bool ShowDebug = false;

        internal static TextWriter logstream;

        public static LogInstance instance = new LogInstance();

        static Log()
        {
            try
            {
                logstream = new StreamWriter(new FileStream("Log.log", FileMode.Append, FileAccess.Write));
            }
            catch
            {
                // Le fichier de log ne semble pas accessible, donc ne pas utiliser de fichier log.
                UseLogFile = false;
                Warning("Log file unavaiable.");
            }
        }

        public static List<TextWriter> DebugStreams = new List<TextWriter> { Console.Out };
        public static List<TextWriter> ErrorStream = new List<TextWriter> { Console.Out };
        public static List<TextWriter> WarningStream = new List<TextWriter> { Console.Out };
        public static List<TextWriter> StandardLog = new List<TextWriter> { Console.Out };

        public static void Debug(string message, params object[] args)
        {
            if (ShowDebug)
            {
                ConsoleColor oldcolor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Gray;

                ErrorStream.ForEach(writer => 
                {
                    writer.WriteLine("[Debug] " + message, args);
                    writer.Flush();
                });

                if (UseLogFile)
                    logstream.WriteLine("[Debug] " + message, args);

                Console.ForegroundColor = oldcolor;
                instance.Raise(string.Format("[Debug] " + message, args), 0);
            }
        }
        public static void Error(string message, params object[] args)
        {
            ConsoleColor oldcolor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;

            ErrorStream.ForEach(writer => 
            {
                writer.WriteLine("[ERROR] " + message, args);
                writer.Flush();
            });

            if (UseLogFile)
                logstream.WriteLine("[ERROR] " + message, args);
            Console.ForegroundColor = oldcolor;

            instance.Raise(string.Format("[ERROR] " + message, args), 3);
        }
        public static void Warning(string message, params object[] args)
        {
            ConsoleColor oldcolor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Yellow;

            ErrorStream.ForEach(writer =>
            {
                writer.WriteLine("[WARNING] " + message, args);
                writer.Flush();
            });

            if (UseLogFile)
                logstream.WriteLine("[WARNING] " + message, args);
            Console.ForegroundColor = oldcolor;

            instance.Raise(string.Format("[WARNING] " + message, args), 2);
        }
        public static void Write(string message, params object[] args)
        {
            ErrorStream.ForEach(writer =>
            {
                writer.WriteLine(message, args);
                writer.Flush();
            });
            if (UseLogFile)
                logstream.WriteLine(message);

            instance.Raise(string.Format(message, args), 1);
        }
    }
    /// <summary>
    /// Instance used for Log callbacks.
    /// </summary>
    public class LogInstance
    {
        public LogInstance()
        {
            OnMessage = (m, l) => {};
        }

        /// <summary>
        /// On message callback.
        /// </summary>
        public delegate void OnMessageCallback(string message, int level);
        /// <summary>
        /// Occurs when on message was written.
        /// </summary>
        public event OnMessageCallback OnMessage;

        public void Raise(string message, int level)
        {
            OnMessage(message, level);
        }
    }
}
