﻿using System;

namespace TXCobalt.Core
{
    [Serializable]
    public struct ShieldObjectData
    {
        public ShieldObjectData(Color color, float level, float absorbtion, float regeneration, bool collide = false)
        {
            Color = color;
            ShieldLevel = level;
            MaxShieldLevel = level;
            Absorbtion = absorbtion;
            RegenerationRate = regeneration;
            Collide = collide;
        }

        public readonly float MaxShieldLevel;
        public readonly bool Collide;
        public float Absorbtion, ShieldLevel, RegenerationRate;
        public Color Color;
    }
}

