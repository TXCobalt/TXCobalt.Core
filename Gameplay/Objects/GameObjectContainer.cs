﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;

namespace TXCobalt.Core
{
    [Serializable]
    /// <summary>
    /// Objet abstrait dont Data est de type dynamique, permet la communication simple des objets entre le client et le serveur.
    /// </summary>
    public struct GameObjectContainer
    {
        #region Constructor

        public GameObjectContainer(GameObject Object)
        {
            TypeId = GetKey(RegisteredValues, Object.Data.GetType());
            transform = Object.transform;
            ObjectData = Object.Data;
            Guid = Object.GUID.ToString();
        }

        #endregion

        #region RegisteredValues

        /// <summary>
        /// Types used for Data in container, used for Serialization/Deserialization.
        /// </summary>
        public static Dictionary<string, Type> RegisteredValues = new Dictionary<string, Type>()
        {
            { "Player", typeof(PlayerObjectData) },
            { "Bullet", typeof(BulletObjectData) },
            { "Shield", typeof(ShieldObjectData) },
        };

        #endregion

        #region Methods

        /// <summary>
        /// Serialize an GameObject.
        /// </summary>
        public static byte[] Serialize(GameObject Object)
        {
            if (Object == null)
                throw new ArgumentNullException("Object");

            GameObjectContainer container = new GameObjectContainer(Object);
            // return SerializeTool.Serialize(container);
            return Serializer.Serialize(container);
        }

        /// <summary>
        /// Serialize an enumeration to an serialized Array of GameObjectContainer.
        /// </summary>
        public static byte[] Serialize(IEnumerable<GameObject> gameobjects)
        {
            if (gameobjects == null)
                throw new ArgumentNullException("gameobjects");

            List<GameObjectContainer> serializedObjects = new List<GameObjectContainer>();
            foreach (GameObject obj in gameobjects)
                serializedObjects.Add(new GameObjectContainer(obj));
            // return SerializeTool.Serialize(serializedObjects.ToArray());
            return Serializer.Serialize(serializedObjects.ToArray());
        }
            
        /// <summary>
        /// Deserialize an array of container or an single container to an List or an GameObjectContainer.
        /// </summary>
        public static object Deserialize(byte[] datas, bool IsList)
        {
            if (datas == null)
                throw new ArgumentNullException("datas");

            if (!IsList)
                #pragma warning disable 618
                return Resolve(Serializer.Deserialize<GameObjectContainer>(datas));
            else
            {
                GameObjectContainer[] objects = Serializer.Deserialize<GameObjectContainer[]>(datas);
                List<GameObjectContainer> outputs = new List<GameObjectContainer>();

                foreach (GameObjectContainer container in objects)
                    outputs.Add(Resolve(container));
                    #pragma warning restore 618

                return outputs.ToArray();
            }
        }

        public static bool TrySerialize(GameObject Object, out byte[] datas)
        {
            try
            {
                datas = Serialize(Object);
                return true;
            }
            catch
            {
                datas = new byte[0];
                return false;
            }
        }

        public static bool TrySerialize(IEnumerable<GameObject> gameobjects, out byte[] datas)
        {
            try
            {
                datas = Serialize(gameobjects);
                return true;
            }
            catch
            {
                datas = null;
                return false;
            }
        }

        public static bool TryDeserialize(byte[] datas, out GameObjectContainer container, bool IsList)
        {
            try
            {
                container = (GameObjectContainer)Deserialize(datas, IsList);
                return true;
            }
            catch
            {
                container = new GameObjectContainer();
                return false;
            }
        }

        public static GameObjectContainer[] Pack(IEnumerable<GameObject> gameobjects)
        {
            if (gameobjects == null)
                throw new ArgumentNullException("gameobjects");

            List<GameObjectContainer> Objects = new List<GameObjectContainer>();
            foreach (GameObject obj in gameobjects)
                Objects.Add(new GameObjectContainer(obj));

            return Objects.ToArray();
        }

        /// <summary>
        /// Resolve ObjectData, usable only when deserialize an GameUpdate.
        /// </summary>
        [Obsolete("Unrequired using NMS")]
        public static GameObjectContainer Resolve(GameObjectContainer Object)
        {
            if (!(Object.ObjectData is Dictionary<string,object>))
                return Object;

            Dictionary<string, object> obj = (Dictionary<string, object>)Object.ObjectData;
            Type type = RegisteredValues[Object.TypeId];
            object Output = ReflectionUtilities.ToObject(obj, type);

            return new GameObjectContainer { Guid = Object.Guid, ObjectData = Output, transform = Object.transform, TypeId = Object.TypeId };
        }

        #region Other

        public static TKey GetKey<TKey, TValue>(Dictionary<TKey, TValue> dictionary, TValue Value)
        {
            List<TKey> KeyList = new List<TKey>(dictionary.Keys);
            foreach (TKey key in KeyList)
                if (dictionary[key].Equals(Value))
                    return key;
            throw new KeyNotFoundException();
        }

        public static TKey[] GetKeys<TKey, TValue>(Dictionary<TKey, TValue> dictionary, TValue Value)
        {
            List<TKey> KeyList = new List<TKey>(dictionary.Keys);
            List<TKey> FoundKeys = new List<TKey>();
            foreach (TKey key in KeyList)
                if (dictionary[key].Equals(Value))
                    FoundKeys.Add(key);
            if (FoundKeys.Count > 0)
                return FoundKeys.ToArray();
            throw new KeyNotFoundException();
        }

        #endregion

        #endregion

        /// <summary>
        /// Position and rotation of the object.
        /// </summary>
        public Transform transform;
        /// <summary>
        /// The object type, use an name like PlayerObject and need to be present in RegisteredValues to be used.
        /// </summary>
        /// <example>Player, Rocket</example>
        public string TypeId;
        /// <summary>
        /// Serialized datas of the object.
        /// </summary>
        public object ObjectData;
        /// <summary>
        /// Object Guid.
        /// </summary>
        public string Guid;

    }
}
