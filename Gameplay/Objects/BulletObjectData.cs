﻿/*
 *  Bullet object data used for GameObjectContainer.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace TXCobalt.Core
{
    [Serializable]
    public struct BulletObjectData
    {
        public BulletObjectData(Color Color, Guid SenderGuid)
        {
            argbcolor = Color.ToArgb();
            senderGuid = SenderGuid.ToString();
        }
        public BulletObjectData(PlayerObject player)
        {
            var playerobjectdata = (PlayerObjectData)player.Data;

            argbcolor = playerobjectdata.color.ToArgb();
            senderGuid = player.GUID.ToString();
        }

        public string senderGuid;
        public int argbcolor;

        public Color color
        {
            get { return Color.FromArgb(argbcolor); }
            set { argbcolor = value.ToArgb(); }
        }
        public Guid SenderGuid
        {
            get { return Guid.Parse(senderGuid); }
            set { senderGuid = value.ToString(); }
        }
    }
}