﻿using System;

namespace TXCobalt.Core
{
    [Serializable]
    /// <summary>
    /// An shield object that block bullets.
    /// </summary>
    /// <remarks>>Untested feature.</remarks>
    public class ShieldObject : GameObject, ITargetable
    {
        public ShieldObject(Color color, float shieldLevel, bool collide = false, float absorbtion = 1f, float regenerationRate = .75f)
        {
            data = new ShieldObjectData(color, shieldLevel, absorbtion, regenerationRate, collide);
        }

        ShieldObjectData data;
        public override object Data
        {
            get
            {
                return data;
            }
            set
            {
                data = (ShieldObjectData)value;
            }
        }
        public override bool IsCollider
        {
            get
            {
                return data.Collide;
            }
        }
        public override string TypeId
        {
            get
            {
                return "EndergiticShield";
            }
        }

        public override void Update(GameInstance instance)
        {
            data.ShieldLevel = Math.Min(data.MaxShieldLevel, data.ShieldLevel + data.RegenerationRate);
        }

        public void OnDamage(float damage)
        {
            // Damage = baseDamage / (Absorbtion²)
            data.ShieldLevel -= damage / (data.Absorbtion * data.Absorbtion);
        }
    }
}

