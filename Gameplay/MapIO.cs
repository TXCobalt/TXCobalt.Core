﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using System.Linq;

namespace TXCobalt.Core
{
    /// <summary>
    /// Homemade utility used to export and import maps.
    /// </summary>
    public static class MapIO
    {
        static readonly byte[] header = { 84, 88, 147, 52 };

        /// <summary>
        /// Exports the map.
        /// </summary>
        /// <returns><c>true</c>, if map was exported, <c>false</c> otherwise.</returns>
        /// <param name = "map">Map to export.</param>
        /// <param name = "stream">Stream to write the map</param>
        public static bool ExportMap(Map map, Stream stream)
        {
            try
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    // Write the header of the map, this is to verify if the map is an TXCobalt map.
                    // The header is an 4 octect signature.
                    writer.Write(header);

                    // Write map informations.
                    writer.Write(map.Author ?? "");
                    writer.Write(map.MapName ?? "");
                    writer.Write(map.MapDesc ?? "");

                    // Write map Length and height
                    writer.Write(map.Width);
                    writer.Write(map.Height);

                    // Write Map spawn coords.
                    writer.Write(map.DefaultSpawn.x);
                    writer.Write(map.DefaultSpawn.y);

                    // Write the map colors
                    writer.Write(map.MapData);

                    // Write the mapdata
                    writer.Write(map.ColorMapData);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Exports an map.
        /// </summary>
        /// <returns><c>true</c>, if map was exported, <c>false</c> otherwise.</returns>
        /// <param name = "map">Map to export.</param>
        /// <param name = "path">Path of the exported map.</param>
        public static bool ExportMap(Map map, string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create))
                return ExportMap(map, fs);
        }

        /// <summary>
        /// Imports a map using an stream.
        /// </summary>
        public static Map ImportMap(Stream stream)
        {
            using (var reader = new BinaryReader(stream))
            {
                if (!reader.ReadBytes(4).SequenceEqual(header))
                    throw new Exception("The map have an invalid signature/header, maybe is the map invalid or corrupted ?");

                string Author, MapName, MapDesc;
                Author = reader.ReadString();
                MapName = reader.ReadString();
                MapDesc = reader.ReadString();

                var Length = reader.ReadInt32();
                var Height = reader.ReadInt32();

                var spawnX = reader.ReadInt32();
                var spawnY = reader.ReadInt32();

                var MapData = reader.ReadBytes(Length * Height);
                var ColorMapData = reader.ReadBytes(Length * Height);

                return new Map(MapData, Length, Height, new Vector2D(spawnX, spawnY), ColorMapData) { Author = Author, MapDesc = MapDesc, MapName = MapName };
            }
        }
        /// <summary>
        /// Imports a map using an file.
        /// </summary>
        public static Map ImportMap(string path)
        {
            if (!File.Exists(path))
                throw new Exception(string.Concat("Map not found at path : ", path));

            using (var stream = new FileStream(path, FileMode.Open))
                return ImportMap(stream);
        }
    }
}

