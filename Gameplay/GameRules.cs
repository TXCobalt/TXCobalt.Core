﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;
using LitJson;

namespace TXCobalt.Core
{
    /// <summary>
    /// Ensemble de règles et de modificateurs du jeux.
    /// </summary>
    [Serializable]
    public struct GameRules
    {
        [JsonIgnore]
        public static GameRules Default
        {
            get
            {
                return new GameRules 
                {
                    AlternativeBotAlgoritm = false,
                    BotAmount = 8,
                    NoClip = false,
                    EnableAntiHack = true,
                    GameTick = 300,
                    MaxGameObject = 64,
                    MaxPlayer = 16,
                    OptimisedBotAlgoritm = true,
                    AdvancedConsoleDebugging = false,
                    PacketTimeOut = 2500,
                    BulletSpeed = 2,
                    BulletDamage = 17.35f
                };
            }
        }


        /// <summary>
        /// Import an Json file containing rules.
        /// </summary>
        public static GameRules ImportGameRules(string Path)
        {
            try
            {
                return JsonMapper.ToObject<GameRules>(File.ReadAllText(Path));
            }
            catch (Exception e)
            {
                Log.Warning("Failled to Import game rules, use default gamerules, {0}.", e.Message);
                return Default;
            }
        }
        /// <summary>
        /// Export the rules to an Json file.
        /// </summary>
        public static void ExportGameRules(string Path, GameRules rules, bool Indented = true)
        {
            try
            {
                using (TextWriter writer = new StreamWriter(File.OpenWrite(Path)))
                {
                    var jsonWriter = new JsonWriter(writer) { PrettyPrint = Indented, Validate = true };
                    JsonMapper.ToJson(rules, jsonWriter);
                }
            }
            catch (Exception e)
            {
                Log.Warning("Failled to Export game rules, {0}.", e.Message);
            }

        }
        /// <summary>
        /// Si les joueurs peuvent traverser les murs.
        /// </summary>
        public bool NoClip;
        /// <summary>
        /// Nombre maximum d'objets de jeux, si la limite est dépassé, l'objet est détruit.
        /// </summary>
        public int MaxGameObject;
        /// <summary>
        /// Nombre maximum de joueurs (Utilisé uniquement en Server dédié ou en Listening (LAN Party)).
        /// </summary>
        public short MaxPlayer;
        /// <summary>
        /// Nombre de bots (ne prend pas en compte MaxPlayer).
        /// </summary>
        public short BotAmount;
        /// <summary>
        /// Temps de tick du jeux.
        /// </summary>
        public short GameTick;
        /// <summary>
        /// Permet de vérifier si les données du client sont correctes.
        /// </summary>
        public bool EnableAntiHack;
        /// <summary>
        /// Utiliser un algoritme alternatif pour l'IA.
        /// </summary>
        public bool AlternativeBotAlgoritm;
        /// <summary>
        /// Ne calcule pas le trajet a chaque update mais a chaque fin de parcours, permet un jeu plus fluide mais une IA moins dynamique pour chaques événements.
        /// </summary>
        public bool OptimisedBotAlgoritm;
        /// <summary>
        /// Delai d'attente pour le recu des packets.
        /// </summary>
        public int PacketTimeOut;
        /// <summary>
        /// Nombre de tuiles que traverse une balle par Update().
        /// </summary>
        public short BulletSpeed;
        /// <summary>
        /// Dégats moyens d'un tir.
        /// </summary>
        public double BulletDamage;
        /// <summary>
        /// Permet l'utilisation d'un Log détaillé.
        /// </summary>
        public bool AdvancedConsoleDebugging;
    }
}