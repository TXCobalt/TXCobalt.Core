﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.IO;

namespace TXCobalt.Core
{
    [Serializable]
    public struct Map
    {

        // Statics variables
        public static readonly Map EmptyMap = new Map(new byte[] {
            128,128,128,128,128,
            128, 0,  0,  0, 128,
            128, 0,  0,  0, 128,
            128, 0,  0,  0, 128,
            128,128,128,128,128
        }, 5, 5, new Vector2D(3, 3));

        // Contructors
        public Map(byte[] mapdata, int Width, int Height, Vector2D defaultspawn)
        {
            if (mapdata.Length != Width * Height)
                throw new InvalidOperationException("Map map Length or Height not corresponding to data array !");

            this = new Map 
            {
                DefaultSpawn = defaultspawn,
                Height = Height,
                Width = Width,
                MapData = mapdata,
                ColorMapData = new byte[Width * Height]
            };
            ColorMapData.Initialize();
        }
        public Map(byte[] mapdata, int Width, int Height, Vector2D defaultspawn, byte[] ColorMap)
        {
            if (mapdata.Length != Width * Height && ColorMap.Length != Width * Height)
                throw new InvalidOperationException("Map map Length or Height not corresponding to data array !");

            this = new Map 
            {
                DefaultSpawn = defaultspawn,
                Height = Height,
                Width = Width,
                MapData = mapdata,
                ColorMapData = ColorMap
            };
        }

        // Fields
        public byte[] MapData;
        public byte[] ColorMapData;

        public int Width;
        public int Height;
        public Vector2D DefaultSpawn;

        public string MapName;
        public string MapDesc;
        public string Author;


        // Properties
        public byte[,] DataArray
        {
            get
            {
                byte[,] datas = new byte[Width, Height];
                for (int y = 0; y < Height; y++)
                    for (int x = 0; x < Width; x++)
                        datas[x, y] = MapData[y * Width + x];
                return datas;
            }
        }
        public byte[,] ColorMap
        {
            get
            {
                byte[,] datas = new byte[Width, Height];
                for (int y = 0; y < Height; y++)
                    for (int x = 0; x < Width; x++)
                        datas[x, y] = ColorMapData[x * Width + y];
                return datas;
            }
        }

        public bool[,] CollideMap
        {
            get
            {
                bool[,] array = new bool[Width, Height];
                for (int x = 0; x < Width; x++)
                    for (int y = 0; y < Height; y++)
                        array[x, y] = DataArray[x, y] > 127;
                return array;
            }
        }


        // Fonctions
        public static Map ImportMap(string path)
        {
            try
            {
                return MapIO.ImportMap(path);
            }
            catch
            {
                Log.Warning("The map {0} is unavailable or invalid, use an empty map.", path);
                return EmptyMap;
            }
        }
        public void ExportMap(string path)
        {
            if (!MapIO.ExportMap(this, path))
                Log.Error("Can't export map at path {0}", path);
        }
    }
}
