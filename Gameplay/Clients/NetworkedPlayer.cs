﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace TXCobalt.Core
{
    public class NetworkedPlayer : Player
    {
        public NetworkedPlayer(Socket socket, ConnectionRequest credidential, int PacketTimeOut)
        {
            GUID = Guid.NewGuid();
            Username = credidential.Username;
            color = credidential.color;
            this.socket = socket;
            PacketTimeout = PacketTimeOut;
        }

        #region NetworkData
        public Socket socket;
        public int PacketTimeout;

        public EndPoint RemoteEndpoint
        {
            get
            {
                return socket.RemoteEndPoint;
            }
        }
        public EndPoint LocalEndpoint
        {
            get
            {
                return socket.LocalEndPoint;
            }
        }
        #endregion

        // Lastest input handled by NMS.
        public InputData? LastInput;

        /// <summary>
        /// Vérifie si le joueur reste connecté.
        /// </summary>
        /// <returns>If the player is connected.</returns>
        public bool Check()
        {
            try
            {
                return socket.Connected;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Récupère toutes les données
        /// </summary>
        /// <returns>Indique si des données ont réusis a être reçues.</returns>
        [Obsolete("Unsupported with NMS.\nUse LastInput variable instead.", true)]
        public InputData? ReciveDatas(int PacketTimeOut)
        {
            var _buffer = new byte[4096];
            try
            {
                socket.ReceiveTimeout = PacketTimeOut;
                int Length = socket.Receive(_buffer);

                List<byte> __buffer = new List<byte>(_buffer);
                byte[] buffer = __buffer.GetRange(0, Length).ToArray();

                try
                {
                    return Serializer.Deserialize<InputData>(buffer);
                }
                catch
                {
                    Log.Warning("Unknow packet is comming from {0} ?", RemoteEndpoint);
                    return null;
                }
            }
            catch (Exception e)
            {
                Log.Warning("Unable to recieve a packet from {0}, the timeout is maybe outdated ? ({1})", RemoteEndpoint, e.Message);
                return null;
            }
        }

        public override InputData? GetInput()
        {
            return LastInput;
        }

        public override void SendUpdate(GameUpdate update)
        {
            /* NMS done that now.
            try
            {
                socket.Send(Serializer.Serialize(update));
            }
            catch
            {
                socket.Disconnect(false);
            }
            */
        }

        public override void Finish()
        {
            socket.Close();
        }


    }
}