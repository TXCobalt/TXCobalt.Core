﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Net.Sockets;

#if !NET45
using System.Threading;
#endif
using System.Threading.Tasks;
using TXCobalt.Core.NMS;

namespace TXCobalt.Core
{
    public class GameInstance
    {
        // Constructor
        public GameInstance(Map map, GameRules rules, NmsSettings? settings = null)
        {
            Rules = rules;
            Players = new List<Player>(Rules.MaxPlayer);

            GameObjects = new List<GameObject>(Rules.MaxGameObject);
            this.map = map;
            Active = true;
            Frozen = false;

            nmsListner = new NmsListner(this, settings);

            nmsListner.OnMessageRecieved += (player, message) =>
            {
                if (message.Flag == MessageFlag.InputData)
                    player.LastInput = (InputData)message.Data;
                else if (message.Flag == MessageFlag.DISCONNECT || message.Flag == MessageFlag.ERROR)
                {
                    Players.Remove(player);
                    Destroy(GetPlayerObject(player));
                }
            };
        }

        // Fields
        public Map map;
        public GameRules Rules = GameRules.Default;

        public List<GameObject> GameObjects;
        public List<Player> Players;

        public bool Active;
        public bool Frozen;

        Stack<GameObject> DestroyingStack = new Stack<GameObject>();
        Stack<GameObject> InstantiatingStack = new Stack<GameObject>();

        NmsListner nmsListner;

        // Properties
        public bool[,] CollideMap
        {
            get
            {
                bool[,] colmap = map.CollideMap;

                foreach (GameObject obj in GameObjects)
                    colmap[obj.transform.Pos.x, obj.transform.Pos.y] |= obj.IsCollider;

                return colmap;
            }
        }

        // Methods
        /// <summary>
        /// Destroy the object at the next update
        /// </summary>
        public void Destroy(GameObject obj)
        {
            DestroyingStack.Push(obj);
        }

        /// <summary>
        /// Summon the object at the next update.
        /// </summary>
        public void Instanciante(GameObject obj)
        {
            if (GameObjects.Count >= Rules.MaxGameObject)
                Log.Warning("Unable to instanciate object {{{0}}}, max object limit reached.", obj.GUID);
            else
                InstantiatingStack.Push(obj);
        }

        /// <summary>
        /// Get a Player with his Object.
        /// </summary>
        public Player GetPlayer(PlayerObject Object)
        {
            foreach (Player pl in Players)
                if (pl.GUID == Object.GUID)
                    return pl;
            throw new Exception("The player is not found in objects.");
        }

        /// <summary>
        /// Get a Player with his Guid.
        /// </summary>
        public Player GetPlayer(Guid guid)
        {
            foreach (Player pl in Players)
                if (pl.GUID == guid)
                    return pl;
            throw new Exception("The player is not found in objects.");
        }

        /// <summary>
        /// Get a PlayerObject with his Player.
        /// </summary>
        public PlayerObject GetPlayerObject(Player player)
        {
            foreach (GameObject playerobj in GameObjects)
                if (playerobj is PlayerObject && playerobj.GUID == player.GUID)
                    return (PlayerObject)playerobj;
            throw new Exception("The player object is not found in objects.");
        }

        /// <summary>
        /// Get a PlayerObject with his Guid.
        /// </summary>
        public PlayerObject GetPlayerObject(Guid guid)
        {
            foreach (GameObject playerobj in GameObjects)
                if (playerobj is PlayerObject && playerobj.GUID == guid)
                    return (PlayerObject)playerobj;
            throw new Exception("The player object is not found in objects.");
        }

        /// <summary>
        /// Send a request for a connection to the server (Local).
        /// </summary>
        public Guid? SendRequest(ConnectionRequest request)
        {
            // Si il y a trop de joueur (cad. limite==nombre_de_joueur)
            if (Players.Capacity == Players.Count)
            {
                Log.Write("Connection denied for {0}, there are too many already connected players.", request.Username);
                return null;
            }
            else
            {
                // La connection est possible si limite!=nombre_de_joueur.
                Log.Write("Connection accepted for {0} with {1} version (#{2})", request.Username, request.Version, request.argbcolor);
                Player pl = new LocalPlayer(request);
                AddPlayer(pl);
                return pl.GUID;
            }
        }

        /// <summary>
        /// Send a request for a connection to the server (Socket/Network).
        /// </summary>
        public void SendRequest(Socket socket, ConnectionRequest request)
        {
            // Si il y a trop de joueur (cad. limite==nombre_de_joueur)
            if (Players.Capacity == Players.Count)
                Log.Write("Connection denied for {0}, there are too many already connected players.", request.Username);
            else
            {
                // La connection est possible si limite!=nombre_de_joueur.
                Log.Write("Connection accepted for {0} with {1} version (#{2})", request.Username, request.Version, request.argbcolor);
                AddPlayer(new NetworkedPlayer(socket, request, Rules.PacketTimeOut));
            }
        }

        /// <summary>
        /// Add a player and instantiante his object.
        /// </summary>
        public Player AddPlayer(Player player)
        {
            Players.Add(player);
            Instanciante(new PlayerObject(map, player));
            return player;
        }

        /// <summary>
        /// Try move the player to an another instance.
        /// </summary>
        /// <param name="target">Targeted player.</param>
        /// <param name="transform">Transform destination of the PlayerObject.</param>
        /// <param name="instance">Target instance</param>
        public bool TryMoveTo(Player target, Transform transform, GameInstance instance)
        {
            try
            {
                if (Players.Exists(new Predicate<Player>(target.Equals)))
                {
                    Players.Remove(target);
                    PlayerObject obj = GetPlayerObject(target);
                    obj.transform = transform;
                    instance.GameObjects.Add(obj);//
                    instance.Players.Add(target);
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get every objects at a specified position.
        /// </summary>
        public GameObject[] GetObjects(int x, int y)
        {
            return GameObjects.FindAll(Obj => Obj.transform.Pos == new Vector2D(x, y)).ToArray();
        }

        public bool TestCollision(int x, int y)
        {
            bool Collide = map.MapData[y * map.Width + x] > 127;
            foreach (GameObject obj in GetObjects(x,y))
                Collide |= obj.IsCollider;
            return Collide;
        }

        public void Update()
        {
            try
            {
                Log.Debug("Update called !");

                #if NET45
                Task tick = Task.Delay(Rules.GameTick);
                #else
                Task tick = Task.Factory.StartNew(() => Thread.Sleep(Rules.GameTick));
                #endif

                lock (DestroyingStack)
                    while (DestroyingStack.Count != 0)
                        GameObjects.Remove(DestroyingStack.Pop());
                
                lock (InstantiatingStack)
                    while (InstantiatingStack.Count != 0)
                        GameObjects.Add(InstantiatingStack.Pop());

                try
                {
                    if (!Frozen)
                        lock (GameObjects)
                            foreach (GameObject obj in GameObjects)
                                obj.Update(this);
                }
                catch (Exception e)
                {
                    Log.Warning("Unable to update entities ? {0}", e.Message);
                }
                // ScriptingInterface.Update();

                /// Distribute packets
                #region Distribute packets
                lock (Players)
                {
                    Players.ForEach(pl => pl.SendUpdate(GetUpdate(pl)));
                    // Send updates to NMS players
                    Players.FindAll(pl => pl is NetworkedPlayer)
                    .ForEach(player => nmsListner.Send((NetworkedPlayer)player, new NetworkMessage { Flag = MessageFlag.Update, Data = GetUpdate(player) }));
                    #endregion
                    /// Get packets from players.
                    #region Get packets
                    Parallel.ForEach(Players, pl =>
                    {
                        InputData? datas = pl is NetworkedPlayer ? ((NetworkedPlayer)pl).LastInput : pl.GetInput();
                        if (datas.HasValue)
                            GetPlayerObject(pl).inputdata = datas.Value;
                        //else if (!(pl is NetworkedPlayer))
                        //    KickPlayer(pl, "Corrupted or unavailable input values.");
                    });
                    // Reset every inputs of NMS players.
                    // Players.FindAll(player => player is NetworkedPlayer).ForEach(netpl => ((NetworkedPlayer)netpl).LastInput = null);
                    #endregion
                    /// Check players.
                    #region Check players
                    // Do not use Directly Players because you can not change a list in foreach and that cause errors in Update().
                    var pls = Players.ToArray();
                    Array.ForEach(pls, pl =>
                    {
                        if (pl is NetworkedPlayer)
                            if (!((NetworkedPlayer)pl).Check())
                                KickPlayer(pl, "Connection lost.");
                    });
                }
                #endregion

                tick.Wait();
                tick.Dispose();
            }
            catch (Exception e)
            {
                Log.Error("An error occured in Update()");
                Log.Error(e.ToString());
            }
        }

        public void Loop()
        {
            while (Active)
                Update();
        }

        /// <summary>
        /// Kick a player and finish it's connection to the server.
        /// </summary>
        public void KickPlayer(Player pl, string reason)
        {
            Log.Write("The player {0} has been kicked for: {1}", pl.Username, reason);
            if (pl is NetworkedPlayer)
                nmsListner.Send((NetworkedPlayer)pl, new NetworkMessage(MessageFlag.KICK, new KickMessage(reason)));
            // Make sure there was no object with the player GUID
            GameObjects.FindAll(Object => Object.GUID == pl.GUID).ForEach(Destroy);
            Players.Remove(pl);

            pl.Finish();
        }

        public GameUpdate GetUpdate(Player pl)
        {
            Vector2D camerapos = GetPlayerObject(pl).transform.Pos;

            return new GameUpdate(GameObjectContainer.Pack(GameObjects), PlayerData.Pack(Players), camerapos, (PlayerObjectData)GetPlayerObject(pl).Data);
        }
    }
}
