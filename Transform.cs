﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace TXCobalt.Core
{
    [Serializable]
    public struct Transform
    {
        public Transform(Vector2D pos, Tilt tilt)
        {
            this = new Transform() { Pos = pos, tilt = tilt };
        }

        public Transform(int x, int y, Tilt tilt)
        {
            this = new Transform() { Pos = new Vector2D(x, y), tilt = tilt };
        }

        public Vector2D Pos;
        public Tilt tilt;

        public override string ToString()
        {
            return string.Format("({0};{1}!{2})", Pos.x, Pos.y, tilt);
        }
    }
}
