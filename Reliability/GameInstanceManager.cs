﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace TXCobalt.Core
{
    /// <summary>
    /// Classe permettant de gérer plusieurs instances de facon asyncrone.
    /// </summary>
    public class GameInstanceManager : IDisposable
    {
        public GameInstanceManager()
        {

        }

        public GameInstance this[int index]
        {
            get
            {
                return GameInstances[index].Item1;
            }
        }

        public List<Tuple<GameInstance, Task>> GameInstances = new List<Tuple<GameInstance, Task>>();

        public List<Player> Players
        {
            get
            {
                List<Player> players = new List<Player>();
                GameInstances.ForEach((instance) => players.AddRange(instance.Item1.Players));
                return players;
            }
        }
        public List<NetworkedPlayer> NetPlayers
        {
            get
            {
                List<NetworkedPlayer> netplayers = new List<NetworkedPlayer>();

                foreach (Tuple<GameInstance, Task> tuple in GameInstances)
                    foreach (NetworkedPlayer player in tuple.Item1.Players.FindAll(_=> _ is NetworkedPlayer))
                        netplayers.Add(player);

                return netplayers;
            }
        }
        public bool Active
        {
            get
            {
                bool state = false;
                foreach (Tuple<GameInstance, Task> tuple in GameInstances)
                    state |= tuple.Item2.Status == TaskStatus.Running;

                return state;
            }
        }
        public GameInstanceManager(IEnumerable<GameInstance> instances)
        {
            foreach (GameInstance instance in instances)
            {
                Task t = new Task(instance.Loop);
                t.Start();

                GameInstances.Add(new Tuple<GameInstance, Task>(instance, t));
            }
        }

        public void AddServer(GameInstance instance)
        {
            Task t = new Task(new Action(instance.Loop));
            GameInstances.Add(new Tuple<GameInstance, Task>(instance, t));
            t.Start();
        }

        public void SendConnectionRequest(Socket socket, ConnectionRequest request, int ID = 0)
        {
            this[ID].SendRequest(socket, request);
        }
        public Guid? SendConnectionRequest(ConnectionRequest request, int ID = 0)
        {
            return this[ID].SendRequest(request);
        }

        public void AddPlayer(Player pl, int ID = 0)
        {
            this[ID].Players.Add(pl);
        }

        public Player GetPlayer(PlayerObject Object)
        {
            foreach (Player player in Players)
                if (player.GUID == Object.GUID)
                    return player;

            throw new Exception("The player is not found in objects.");
        }
        public Player GetPlayer(Guid guid)
        {
            foreach (Player player in Players)
                if (player.GUID == guid)
                    return player;
            throw new Exception("The player is not found in objects.");
        }

        public PlayerObject GetPlayerObject(Guid guid)
        {
            foreach (var instance in GameInstances)
                try
                {
                    return instance.Item1.GetPlayerObject(guid);
                }
                catch { }

            throw new Exception("The player object is not found in objects.");
        }
        public PlayerObject GetPlayerObject(Player player)
        {
            foreach (var instance in GameInstances)
                try
                {
                    return instance.Item1.GetPlayerObject(player);
                }
                catch { }
            throw new Exception("The player object is not found in objects.");
        }

        /// <summary>
        /// Return the ID of the gameinstance where is the player or the object.
        /// </summary>
        public int WhereIs(Guid guid)
        {
            for (int i = 0; i < GameInstances.Count; i++)
            {
                try
                {
                    GameInstances[i].Item1.GetPlayerObject(guid);
                    return i;
                }
                catch { }
                try
                {
                    GameInstances[i].Item1.GetPlayer(guid);
                    return i;
                }
                catch { }
            }
            throw new Exception("The Object or Player is located anywhere.");
        }
        /// <summary>
        /// Return the ID of the gameinstance where is the player or the object.
        /// </summary>
        public int WhereIs(Player player)
        {
            for (int i = 0; i < GameInstances.Count; i++)
            {
                try
                {
                    GameInstances[i].Item1.GetPlayerObject(player.GUID);
                    return i;
                }
                catch { }
                try
                {
                    GameInstances[i].Item1.GetPlayer(player.GUID);
                    return i;
                }
                catch { }
            }
            throw new Exception("The Object or Player is located anywhere.");
        }
        /// <summary>
        /// Return the ID of the gameinstance where is the player or the object.
        /// </summary>
        public int WhereIs(GameObject Object)
        {
            for (int i = 0; i < GameInstances.Count; i++)
            {
                try
                {
                    GameInstances[i].Item1.GetPlayerObject(Object.GUID);
                    return i;
                }
                catch { }
            }
            throw new Exception("The Object or Player is located anywhere.");
        }

        public void Dispose()
        {
            Dispose(true);
        }

        internal void Dispose(bool dispose)
        {
            if (dispose)
                GameInstances.RemoveAll(_ => true);

            GameInstances.ForEach(tasks => tasks.Item2.Dispose());
            Players.ForEach(pl => pl.Finish());
        }
        public bool IsPresent(string username)
        {
            return Players.Exists(player => player.Username == username);
        }
    }
}