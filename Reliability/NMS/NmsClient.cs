﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace TXCobalt.Core.NMS
{
    public class NmsClient : IDisposable
    {
        public NmsClient(Socket socket, NmsSettings? settings = null)
        {
            this.settings = settings.GetValueOrDefault();
           
            this.socket = socket;
            recievingTask = new Task(Recieve);
            recievingTask.Start();
        }

        public readonly NmsSettings settings;
        Socket socket;

        const int MaxPacketLength = ushort.MaxValue;
        Task recievingTask;

        public delegate void MessageRecievedDelegate(NetworkMessage message);

        /// <summary>
        /// Event called when a network message is recived.
        /// </summary>
        public event MessageRecievedDelegate OnMessageRecieved;

        public void Recieve()
        {
            while (socket.Connected)
            {
                int count = 0;

                // Server have datas to be read.
                if (socket.Available > 0)
                    //TODO: Catch socket exeptions.
                    while (socket.Available > 0 && count != settings.MaxProcessedMessages)
                        using (var stream = new NetworkStream(socket))
                        {
                            // Read packet size
                            byte[] buffer = new byte[2];
                            stream.Read(buffer, 0, 2);
                            ushort length = BitConverter.ToUInt16(buffer, 0);

                            // Read message
                            byte[] message = new byte[length];
                            stream.Read(message, 0, length);

                            NetworkMessage nm = NetworkMessage.Deserialize(message);
                            Log.Debug(string.Format("Message recieved {0} {1}", nm.Flag, DateTime.Now.ToLongTimeString()));
                            OnMessageRecieved(nm);
                            count++;
                        }
                // Did client have problems ?
                else
                    //TODO: Complete troubleshoting.
                    Log.Debug("[NMS] Server has no datas !");

                // Use delay to use less power
                Thread.Sleep(settings.CheckingDelay);
            }
            Log.Warning("[NMS] Socket is not connected anymore");
        }
        public void Send(NetworkMessage message)
        {
            byte[] binary_message = NetworkMessage.Serialize(message);
            int length = binary_message.Length;
            // Send packet length
            socket.Send(BitConverter.GetBytes((ushort)length));
            // Send message
            socket.Send(binary_message);
        }

        public void Dispose()
        {
            recievingTask.Dispose();
        }
    }
}