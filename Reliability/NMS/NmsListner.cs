﻿/*
 *  This file is a part of TXCobalt game base.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace TXCobalt.Core.NMS
{
    public class NmsListner : IDisposable
    {
        public NmsListner(GameInstance instance, NmsSettings? settings = null)
        {
            if (!settings.HasValue)
                settings = new NmsSettings();

            NmsSettings = settings.Value;

            listeningTask = new Task(() => Listen(instance));
            BeforeSocketCheck = () =>
            {
            };
            listeningTask.Start();
        }

        /// <summary>
        /// Maximum length of packets.
        /// </summary>
        const int MaxPacketLength = ushort.MaxValue;

        /// <summary>
        /// Event called when a network message is recived.
        /// </summary>
        public delegate void MessageRecievedDelegate(NetworkedPlayer player,NetworkMessage message);

        public event Action BeforeSocketCheck;
        public event MessageRecievedDelegate OnMessageRecieved;

        public readonly NmsSettings NmsSettings;

        Task listeningTask;

        public void Listen(GameInstance instance)
        {
            while (true)
            {
                BeforeSocketCheck();
                // Check every sockets for data
                List<Player> netplayers = instance.Players.FindAll(_ => _ is NetworkedPlayer);
                Parallel.ForEach(netplayers, player =>
                {
                    var Client = player as NetworkedPlayer;
                    var socket = Client.socket;

                    // Kick an disconnected socket.
                    if (!socket.Connected)
                        instance.KickPlayer(player, "Socket disconnection.");

                    // Don't send data to an disconnected or blocking socket.
                    if (socket.Blocking)
                        return;
                    
                    try
                    {
                        int count = 0;

                        // Client have datas to be read.
                        if (socket.Available > 0)
                            // Check if the socket is available (reduce socket exception when error or disconnecting)
                            // and check if the limit was not reached.
                            while (socket.Available > 0 && count != NmsSettings.MaxProcessedMessages)
                                //TODO: Catch socket exeptions.
                                using (var stream = new NetworkStream(socket))
                                {
                                    // Read packet size
                                    byte[] buffer = new byte[2];
                                    stream.Read(buffer, 0, 2);
                                    ushort length = BitConverter.ToUInt16(buffer, 0);

                                    // Read message
                                    byte[] message = new byte[length];
                                    stream.Read(message, 0, length);

                                    NetworkMessage nm = NetworkMessage.Deserialize(message);
                                    OnMessageRecieved(Client, nm);
                                    count++;
                                }
                        // Did client have problems ?
                        else
                        {
                            //TODO: Complete troubleshoting.
                            Log.Debug("[NMS] Client has no datas !");
                        }
                    }
                    catch (TimeoutException)
                    {
                        instance.KickPlayer(player, "Timeout.");
                    }
                    catch (Exception e)
                    {
                        Log.Error("[NMS] Error occured in listner : {0}", e);
                    }

                });
                // Use delay to use less power.
                Thread.Sleep(NmsSettings.CheckingDelay);
            }
        }

        public void Send(NetworkedPlayer player, NetworkMessage message)
        {
            try
            {
                byte[] binary_message = NetworkMessage.Serialize(message);
                int length = binary_message.Length;
                // Send packet length
                player.socket.Send(BitConverter.GetBytes((ushort)length));
                // Send message
                player.socket.Send(binary_message);
            }
            catch (SocketException e)
            {
                Log.Warning("[NMS] Player {0} has lost connection : {1}", player.Username, e.Message);
            }
            catch (Exception e)
            {
                Log.Error("[NMS] NMS error occured : {0}", e);
                SendError(player, e.Message, 1);
            }
        }

        static void SendError(NetworkedPlayer player, string message, int error)
        {
            try
            {
                byte[] binary_message = NetworkMessage.Serialize(new NetworkMessage(MessageFlag.ERROR, new ErrorMessage(message, error)));
                int length = binary_message.Length;
                // Send packet length
                player.socket.Send(BitConverter.GetBytes((ushort)length));
                // Send message
                player.socket.Send(binary_message);
            }
            catch
            {
                Log.Error("[NMS] Can't send NMS error to user.");
            }
        }

        public void Dispose()
        {
            listeningTask.Dispose();
        }
    }
}